<%-- 
    Document   : result
    Created on : Nov 25, 2019, 9:17:31 AM
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Product List of search<s:property value="keyword"/></title>
    </head>
    <body>
        <h1>Product List of search <s:property value="keyword"/></h1>
        <table style="border: 1px solid black">
            <s:iterator value="products" var="product">
                <tr>
                    <td style="border: 1px solid black"><s:property value="name"/></td>
                    <td style="border: 1px solid black"><s:property value="price"/></td>
                    <td style="border: 1px solid black"><s:property value="description"/></td>
                    <td style="border: 1px solid black"><a href="addToCart?newProductId=<s:property value="id"/>">Add to cart</a></td>
                </tr>
            </s:iterator>
            <a href="index.jsp">Back</a>
        </table>
    </body>
</html>
